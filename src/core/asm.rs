use super::instr_set;
use super::instr::Instr;
use super::util::to_hex;

use std::fmt;

pub enum AsmLine{
    I(Instr),
    Label{name: String, pos: u8},
    Origin(u8),
    Data(Vec<u8>),
}
use self::AsmLine::*;

impl fmt::Display for AsmLine{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            I(ref x)             => write!(f,"  {}",x.clone()),
            Label{name:ref n,..} => write!(f,"{}:",n.clone()),
            Origin(x)            => write!(f,"  ORG {:X}",x),
            Data(ref x)          => {
                if x.len() == 1{
                    write!(f,"  DB {}",to_hex(x))
                } else {
                    let mut s = String::new();
                    for c in x { s.push(*c as char); }
                    write!(f,"  DB \"{}\"",s)
                }
            }
        }
    }
}

pub struct Asm{
    pub is: instr_set::InstrSet,
    data: Vec<AsmLine>,
}

impl Asm{
    pub fn new() -> Asm{
        Asm{
            is: instr_set::InstrSet::new(),
            data: Vec::<AsmLine>::new(),
        }
    }

    pub fn parse(&mut self, asm: &Vec<String>){

        // Make all instructions and values uppercase, trim
        // whitespace, and remove comments
        let mut lines = Vec::<String>::new();
        for line in asm{
            let mut l = line.to_uppercase().trim().to_owned();
            if l.starts_with("DB"){
                l = format!("DB{}",&line.trim()[2..]);
            }
            if let Some(p) = l.find(";"){l.truncate(p)}
            l = l.trim().to_owned();
            if l.len() > 0{
                lines.push(l);
            }
        }

        let mut labels = Vec::<(String,u8)>::new();
        let mut pos = 0 as u8;
        let mut to_set = false;
        let mut prev_label = String::new();

        for l in &lines {
            if l.contains(":") {
                prev_label = l.clone();
                prev_label.truncate(l.len() - 1); //remove ':'
                to_set = true;
            } else {
                let mut pre = false;

                if l.starts_with("DB"){
                    pre = true;
                    let x = l[2..].trim();
                    if x.starts_with("\""){ pos += x.len() as u8 - 2; }
                    else{ pos += 1; }
                }
                else if l.starts_with("ORG"){
                    pre = true;
                    let x = u8::from_str_radix(l[3..].trim(), 16).expect(
                        &format!("Parse error at {}",l)[..]);
                    pos = x;
                }
                if !pre {
                    if to_set {
                        to_set = false;
                        labels.push((prev_label.clone(),pos));
                    }
                    pos += l.replace(","," ").split_whitespace().count() as u8;
                }
            }
        }

        let mut pos = 0 as u8;
        let mut cur_label = 0 as usize;
        for l in &mut lines {
            if l.contains(":") {
                let &(ref n,ref p) = &labels[cur_label];
                cur_label += 1;
                self.data.push(Label{name: n.clone(), pos: *p});
            }
            else if l.starts_with("DB"){
                let mut x = l[2..].trim();
                let mut d = Vec::<u8>::new();
                if x.starts_with("\""){
                    x = &x[1 .. x.len() -1];
                    for c in x.as_bytes(){
                        d.push(*c);
                    }
                    pos += x.len() as u8;
                }
                else{
                    let x = u8::from_str_radix(l[3..].trim(), 16).expect(
                        &format!("Parse error at {}",l)[..]);
                    d.push(x);
                    pos += 1;
                }
                self.data.push(Data(d));
            }
            else if l.starts_with("ORG"){
                if let Ok(x) = u8::from_str_radix(l[3..].trim(), 16){
                    self.data.push(Origin(x));
                    pos = x;
                } else {
                    panic!("error parsing hex value at {}", l)
                }
            }
            else{
                if l.starts_with("J"){
                    for x in &labels{
                        let &(ref name,ref p) = x;
                        let ps = to_hex(&vec![p.wrapping_sub(pos)]);
                        *l = l.replace(&name[..],&ps[..]);
                    }
                }
                let mut tmp = Instr::from_asm(&self.is, &l[..]);
                tmp.pos = pos;
                pos += tmp.to_bin().len() as u8;
                self.data.push(I(tmp));
            }
        }
    }

    pub fn to_bin(&self) -> Vec<u8>{
        let mut data = Vec::<u8>::new();
        for i in &self.data{
            match *i{
            I(ref x)    => data.append(&mut x.to_bin()),
            Label{..}   => continue,
            Origin(x)   => while data.len() < x as usize { data.push(0u8); },
            Data(ref x) => data.append(&mut x.clone()),
            }
        }
        data
    }

    pub fn data(&self) -> &Vec<AsmLine>{
        &self.data
    }
}

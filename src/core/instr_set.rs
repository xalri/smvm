use std::rc::Rc;

use super::instr::*;
use super::vm;
use super::instr::AddrMode::*;

pub struct InstrSet{
    pub is: Vec<Rc<Op>>,
}

fn mov() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = vm.get(i.arg(1));
        vm.set(i.arg(0), v);
    })
}
fn mod2<F: Fn(i16,i16)->i16 + 'static>(f: F) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = f(vm.get(i.arg(0)) as i16, vm.get(i.arg(1)) as i16);
        if v < 0x00 || v > 0xFF { vm.setf(1,true); }
                           else { vm.setf(1,false);}
        vm.set(i.arg(0), v as u8);
    })
}
fn mod1<F: Fn(i16)->i16 + 'static>(f: F) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = f(vm.get(i.arg(0)) as i16);
        if v < 0x00 || v > 0xFF { vm.setf(1,true); }
                           else { vm.setf(1,false);}
        vm.set(i.arg(0),v as u8);
    })
}
fn jmp(n: bool, flag: u8, cond: bool) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        if !cond || (n ^ vm.getf(flag)) {
            let off = vm.get(i.arg(0));
            vm.jump(off);
        }
    })
}
fn cmp() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = vm.get(i.arg(0)) as i16 - vm.get(i.arg(1)) as i16;
        vm.setf(0, v < 0);
        vm.setf(2, v == 0);
    })
}
fn call(int: bool) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let pos = vm.pos() + 2;
        vm.stack_push(pos);
        let v = if int { vm.get(Arg{mode:AddrMode::Dir, value:i.argv[0]}) }
                   else{ vm.get(i.arg(0)) };
        vm.goto(v);
    })
}
fn ret(_int: bool) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |_i,vm|{
        let pos = vm.stack_pop();
        vm.goto(pos);
    })
}
fn push(f: bool) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = if f { vm.reg[5] } else {vm.get(i.arg(0))};
        vm.stack_push(v);
    })
}
fn pop(f: bool) -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{
        let v = vm.stack_pop();
        if f { vm.reg[5] = v; } else {vm.set(i.arg(0), v);}
    })
}
fn noop() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |_i,_vm|{})
}
fn pin() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{ vm.pin(i.arg(0));})
}
fn pout() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |i,vm|{ vm.pout(i.arg(0));})
}
fn end() -> Box<Fn(&Instr,&mut vm::VM)>{
    Box::new(move |_i,vm|{ vm.done = true; })
}

impl InstrSet{
    pub fn from_asm(&self, name: &str, argt: Vec<AddrMode>) -> Rc<Op>{
        let mut filter = self.is.iter()
            .filter(|&i|{ i.name == name })
            .filter(|&i|{ i.argt.len() == argt.len() })
            .filter(|&i|{ for j in 0..argt.len(){
                                    if i.argt[j] != argt[j]{return false;}
                                 } true });
        if let Some(a) = filter.next() { a.clone() }
        else { panic!("No match for ASM {} {:?}", name, argt); }
    }

    pub fn from_bin(&self, opcode: u8) -> Rc<Op>{
        let mut filter = self.is.iter()
            .filter(|&i|{ i.opcode == opcode });

        if let Some(a) = filter.next() { a.clone() }
        else { panic!("No match for opcode {}", opcode); }
    }

    pub fn argn(&self, name: &str) -> usize{
        let mut filter = self.is.iter()
            .filter(|&i|{ i.name == name });

        if let Some(a) = filter.next() { a.argt.len() }
        else { panic!("No Instruction with name {}", name); }
    }

    pub fn new()-> InstrSet{
        let tmp = vec![
            Op::new("MOV",0xD0,vec!{Reg,Imm}, mov()),
            Op::new("MOV",0xD1,vec!{Reg,Dir}, mov()),
            Op::new("MOV",0xD2,vec!{Dir,Reg}, mov()),
            Op::new("MOV",0xD3,vec!{Reg,Ind}, mov()),
            Op::new("MOV",0xD4,vec!{Ind,Reg}, mov()),

            Op::new("ADD",0xA0,vec!{Reg,Reg}, mod2(|a,b|{a+b})),
            Op::new("SUB",0xA1,vec!{Reg,Reg}, mod2(|a,b|{a-b})),
            Op::new("MUL",0xA2,vec!{Reg,Reg}, mod2(|a,b|{a*b})),
            Op::new("DIV",0xA3,vec!{Reg,Reg}, mod2(|a,b|{a/b})),
            Op::new("INC",0xA4,vec!{Reg},     mod1(|a|{a+1})),
            Op::new("DEC",0xA5,vec!{Reg},     mod1(|a|{a-1})),

            Op::new("AND",0xAA,vec!{Reg,Reg}, mod2(|a,b|{a&b})),
            Op::new("OR" ,0xAB,vec!{Reg,Reg}, mod2(|a,b|{a|b})),
            Op::new("XOR",0xAC,vec!{Reg,Reg}, mod2(|a,b|{a^b})),
            Op::new("NOT",0xAD,vec!{Reg},     mod1(|a|{!a})),
            Op::new("ROL",0x9A,vec!{Reg},     mod1(|a|{(a<<1) & (a>>7)})),
            Op::new("ROR",0x9B,vec!{Reg},     mod1(|a|{(a>>1) & (a<<7)})),
            Op::new("SHL",0x9C,vec!{Reg},     mod1(|a|{a<<1})),
            Op::new("SHR",0x9D,vec!{Reg},     mod1(|a|{a>>1})),

            Op::new("ADD",0xB0,vec!{Reg,Imm}, mod2(|a,b|{a+b})),
            Op::new("SUB",0xB1,vec!{Reg,Imm}, mod2(|a,b|{a-b})),
            Op::new("MUL",0xB2,vec!{Reg,Imm}, mod2(|a,b|{a*b})),
            Op::new("DIV",0xB3,vec!{Reg,Imm}, mod2(|a,b|{a/b})),
            Op::new("AND",0xBA,vec!{Reg,Imm}, mod2(|a,b|{a&b})),
            Op::new("OR" ,0xBB,vec!{Reg,Imm}, mod2(|a,b|{a|b})),
            Op::new("XOR",0xBC,vec!{Reg,Imm}, mod2(|a,b|{a^b})),

            Op::new("JMP",0xC0,vec!{Imm},     jmp(false,0,false)),
            Op::new("JZ" ,0xC1,vec!{Imm},     jmp(false,0,true)),
            Op::new("JNZ",0xC2,vec!{Imm},     jmp(true, 0,true)),
            Op::new("JS" ,0xC3,vec!{Imm},     jmp(false,2,true)),
            Op::new("JNS",0xC4,vec!{Imm},     jmp(true, 2,true)),
            Op::new("JO" ,0xC5,vec!{Imm},     jmp(false,1,true)),
            Op::new("JNO",0xC6,vec!{Imm},     jmp(true, 1,true)),

            Op::new("CMP",0xDA,vec!{Reg,Reg}, cmp()),
            Op::new("CMP",0xDB,vec!{Reg,Imm}, cmp()),
            Op::new("CMP",0xDC,vec!{Reg,Dir}, cmp()),

            Op::new("CALL",0xCA,vec!{Imm},    call(false)),
            Op::new("RET" ,0xCB,vec!{},       ret(false)),
            Op::new("INT" ,0xCC,vec!{Imm},    call(true)),
            Op::new("IRET",0xCD,vec!{},       ret(true)),

            Op::new("PUSH", 0xE0,vec!{Reg},   push(false)),
            Op::new("POP",  0xE1,vec!{Reg},   pop(false)),
            Op::new("PUSHF",0xEA,vec!{Reg},   push(true)),
            Op::new("POPF", 0xEB,vec!{Reg},   pop(true)),

            Op::new("IN",  0xF0,vec!{Imm},    pin()),
            Op::new("OUT", 0xF1,vec!{Imm},    pout()),
            Op::new("CLO", 0xFE,vec!{},       noop()),
            Op::new("END", 0x00,vec!{},       end()),
            Op::new("NOP", 0xFF,vec!{},       noop()),
            Op::new("STI", 0xFC,vec!{}, Box::new(move|_i,vm|{vm.setf(3,true);})),
            Op::new("CLI", 0xFD,vec!{}, Box::new(move|_i,vm|{vm.setf(3,false);})),

        ];
        let mut is = Vec::<Rc<Op>>::new();
        for o in tmp{
            is.push(Rc::new(o));
        }
        InstrSet{is: is}
    }
}

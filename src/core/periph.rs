use std::fmt;

pub trait Periph{
    fn before_step(&mut self){}
    fn after_step(&mut self){}
    fn start(&mut self){}
    fn receive(&mut self, v: u8);
    fn send(&mut self) -> u8;
}

pub struct Lcd{
    segments: [[bool; 7]; 2],
}

impl Lcd{
    pub fn new() -> Lcd{
        Lcd{ segments: [[false; 7]; 2] }
    }

    fn print(&self){
        let s = self.segments;
        println!(" {}    {} \n{}{}{}  {}{}{}\n{}{}{}  {}{}{}",
            if s[0][6]{'_'}else{' '},
            if s[1][6]{'_'}else{' '},

            if s[0][5]{'|'}else{' '},
            if s[0][1]{'_'}else{' '},
            if s[0][0]{'|'}else{' '},
            if s[1][5]{'|'}else{' '},
            if s[1][1]{'_'}else{' '},
            if s[1][0]{'|'}else{' '},

            if s[0][4]{'|'}else{' '},
            if s[0][3]{'_'}else{' '},
            if s[0][2]{'|'}else{' '},
            if s[1][4]{'|'}else{' '},
            if s[1][3]{'_'}else{' '},
            if s[1][2]{'|'}else{' '});
    }
}

impl Periph for Lcd{
    fn receive(&mut self, v: u8){
        {
            let target = &mut self.segments[(v & 0x1) as usize];
            *target = [
                v & 0b00000010 != 0,
                v & 0b00000100 != 0,
                v & 0b00001000 != 0,
                v & 0b00010000 != 0,
                v & 0b00100000 != 0,
                v & 0b01000000 != 0,
                v & 0b10000000 != 0];
        }
        self.print();
    }
    fn send(&mut self) -> u8{
        0
    }
}

impl fmt::Display for Light{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        write!(f, "|{}|{}|{}|",
               if self.red   {"R"} else {" "},
               if self.amber {"A"} else {" "},
               if self.green {"G"} else {" "})
    }
}

struct Light{ red: bool, amber: bool, green: bool, }
impl Light{
    fn new(r: bool, a: bool, g:bool) -> Light{
        Light{red:r,amber:a,green:g}
    }
}

pub struct Lights{
    light: (Light,Light),
    raw: u8,
}

impl Lights{
    pub fn new() -> Lights{
        Lights{ light: (Light::new(false,false,false),
                        Light::new(false,false,false)),
                raw: 0,}
    }
}

impl Periph for Lights{
    fn receive(&mut self, v: u8){
        self.raw = v;
        self.light = (Light::new( v & 0b10000000 != 0,
                                  v & 0b01000000 != 0,
                                  v & 0b00100000 != 0),
                      Light::new( v & 0b00010000 != 0,
                                  v & 0b00001000 != 0,
                                  v & 0b00000100 != 0));

        println!("{}   {}",self.light.0, self.light.1);
    }
    fn send(&mut self) -> u8{
        self.raw
    }
}

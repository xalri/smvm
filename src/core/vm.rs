use super::instr;
use super::instr_set;
use super::instr::AddrMode::*;
use super::util::*;
use super::periph::*;

use std::io;
use std::io::Read;
use std::thread;
use std::time::Duration;


pub struct VM{
    pub reg: Vec<u8>,
    ram: Vec<u8>,
    is: instr_set::InstrSet,
    pub done: bool,
    periph: Vec<Box<Periph>>,
    last_vdu: Vec<u8>,
}

impl VM{
    pub fn new() -> VM{
        VM{
            reg: vec![0,0,0,0,0,0,0xBF],
            ram: vec![0; 0x100],
            done: false,
            is: instr_set::InstrSet::new(),
            periph: vec![
                Box::new(Lights::new()),
                Box::new(Lcd::new()),
            ],
            last_vdu: vec![0; 0xFF - 0xC0],
        }
    }

    pub fn load(&mut self, data: &[u8]){
        if data.len() > 0x100 { panic!("VM::load, data too large for RAM!"); }
        self.ram[0..data.len()].copy_from_slice(data);
    }

    pub fn step(&mut self) -> Option<instr::Instr>{
        if self.done { return None; }
        let opcode = self.ram[self.reg[4] as usize];

        let mut i = instr::Instr::from_bin(&self.is, opcode);
        i.pos = self.reg[4];
        for tmp in 0 .. i.argv.len(){
            i.argv[tmp] = self.ram[(self.reg[4] as usize) + tmp + 1];
        }
        let i = i;

        let prev_ip = self.reg[4];

        i.run(self);

        if self.reg[4] == prev_ip {
            self.reg[4] += i.len() as u8;
        }
        Some(i)
    }

    pub fn sleep(&self, sleep: Duration){
        thread::sleep(sleep);
    }

    pub fn reg_string(&self) -> String{ to_hex(&self.reg) }
    pub fn ram_string(&self) -> String{ hex_table(&self.ram) }

    pub fn disp_vdu(&mut self){
        let vdu = self.ram[0xC0..0xFF].to_vec();
        if vdu != self.last_vdu{
            let mut s = String::new();
            for b in &vdu{
                s.push(*b as char);
            }
            println!("VDU: {}", s);
            self.last_vdu = vdu;
        }
    }

    pub fn set(&mut self, addr: instr::Arg, val: u8){
        let v = addr.value as usize;
        match addr.mode{
            Reg => { self.reg[v] = val; },
            Ind => { self.ram[self.reg[v] as usize] = val; },
            Imm => { panic!("Tried to set an immediate argument o.O"); },
            Dir => { self.ram[v] = val; },
        }
    }
    pub fn get(&self, addr: instr::Arg) -> u8{
        let v = addr.value as usize;
        match addr.mode{
            Reg => { self.reg[v] },
            Ind => { self.ram[self.reg[v] as usize] },
            Imm => { addr.value },
            Dir => { self.ram[v] },
        }
    }
    pub fn getf(&self, flag: u8) -> bool{
        if flag > 3 { panic!("tried to get nonexistant flag"); }
        let mask = !(1u8 << flag);
        (self.reg[5] & mask) != 0
    }
    pub fn setf(&mut self, flag: u8, v: bool){
        if flag > 3 { panic!("tried to get nonexistant flag"); }
        let mask = !(1u8 << flag);
        self.reg[5] = (self.reg[5] & mask) | (if v{1u8}else{0u8} << flag);
    }
    pub fn pos(&self) -> u8{ self.reg[4] }

    pub fn goto(&mut self, pos: u8){ self.reg[4] = pos; }

    pub fn jump(&mut self, pos: u8){ self.reg[4] = self.reg[4].wrapping_add(pos); }

    fn keyboard_in(&mut self){
        println!("Waiting for keyboard input..");
        let mut b = io::stdin().bytes().next().unwrap()
            .expect("Failed to read from stdin");

        if b == 0x0A { b = 0x0D; } // Yay, windows.
        self.reg[0] = b;
    }

    pub fn pin(&mut self, arg: instr::Arg){
        if arg.value == 0{
            self.keyboard_in();
            return;
        }
        if (arg.value-1) as usize > self.periph.len() {
            panic!("tried to acces unimplemented peripheral");
        }
        let v = self.periph[(arg.value - 1) as usize].send();
        self.reg[0] = v;
    }
    pub fn pout(&mut self, arg: instr::Arg){
        if (arg.value-1) as usize > self.periph.len() {
            panic!("tried to acces unimplemented peripheral");
        }
        let v = self.reg[0];
        self.periph[(arg.value - 1) as usize].receive(v);
    }

    pub fn stack_push(&mut self, val: u8){
        self.ram[(self.reg[6]) as usize] = val;
        self.reg[6] -= 1;
    }
    pub fn stack_pop(&mut self) -> u8{
        self.reg[6] += 1;
        let val = self.ram[(self.reg[6]) as usize];
        val
    }
}

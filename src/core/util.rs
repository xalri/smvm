
pub fn to_hex(bytes: &Vec<u8>) -> String{
    let s = bytes.iter()
        .map(|b|{format!("{:02X}",b)}).collect::<Vec<_>>();
    s.join(" ")
}

pub fn hex_table(bytes: &Vec<u8>) -> String{
    let string = to_hex(bytes);
    let data: Vec<&str> = string.split_whitespace().collect();
    let mut col = 0;
    let mut row = 0 as usize;
    let mut ret = String::new();

    ret.push_str("    ");
    for i in 0x0..0x10{
        ret.push_str(&to_hex(&vec![i])[..]);
        ret.push(' ');
    }
    ret.push_str("\n00  ");

    for b in &data{
        ret.push_str(b);
        ret.push(' ');
        col += 1;
        if col > 15 {
            col = 0;
            row += 0x10;
            ret.push_str("\n");
            ret.push_str(&to_hex(&vec![row as u8])[..]);
            ret.push_str("  ");
        }
    }
    ret
}

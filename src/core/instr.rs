use std::cmp::min;
use std::fmt;
use std::rc::Rc;

use super::vm;
use super::instr_set;
use super::instr_set::InstrSet;
use super::util::to_hex;

#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Clone)]
pub enum AddrMode{
    Reg, Ind, Imm, Dir,
}
use self::AddrMode::*;

pub struct Arg{
    pub mode: AddrMode,
    pub value: u8,
}

impl Arg{
    pub fn new(mode: AddrMode, val: u8) -> Arg{
        Arg{ mode: mode, value: val}
    }

    pub fn from_asm(s: &str) -> Arg{
        if s.len() == 0 {panic!("empty string passed to Arg::from_asm");}

        let indr = s.starts_with('[');
        let reg = s.contains('L');

        let start = if indr {1}         else {0};
        let len =   if indr {s.len()-1} else {s.len()};
        let end =   min(len , start + if reg {1} else {2});

        if let Ok(val) = u8::from_str_radix( &s[start .. end] ,16) {
            Arg{ mode: if indr { if reg {Ind}  else {Dir}
                        } else { if reg {Reg}  else {Imm}},
                  value: if reg { val-0xA } else { val } }
        } else { panic!("parse error at '{}'",s); }
    }

    pub fn to_asm(&self) -> String{
        let mut asm;
        let (indr, reg) = match self.mode{
            Reg => (false, true),
            Ind => (true,  true),
            Imm => (false, false),
            Dir => (true,  false)
        };
        if reg { asm = to_hex(&vec![self.value + 0xA])[1..2].to_owned() + "L"; }
        else   { asm = to_hex(&vec![self.value]); }
        if indr{ asm.insert(0,'['); asm.push(']'); }
        asm
    }

    pub fn split(args: &Vec<Arg>) -> (Vec<AddrMode>,Vec<u8>){
        let mut argt = Vec::<AddrMode>::new();
        let mut argv = Vec::<u8>::new();
        for ref a in args {
            argt.push(a.mode.clone());
            argv.push(a.value.clone());
        }
        (argt, argv)
    }
}

pub struct Op{
    pub name: &'static str,
    pub opcode: u8,
    pub argt: Vec<AddrMode>,
    _run: Box<Fn(&Instr, &mut vm::VM)>,
}

impl Op{
    pub fn new(n: &'static str, op: u8, a: Vec<AddrMode>,
               r: Box<Fn(&Instr, &mut vm::VM)>) -> Op{
        Op{name: n, opcode: op, argt: a, _run: r}
    }
}

pub struct Instr{
    pub op: Rc<Op>,
    pub argv: Vec<u8>,
    pub pos: u8,
}

impl fmt::Display for Instr{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        write!(f, "{:016}; [{}] {}",
               self.to_asm(),
               to_hex(&vec![self.pos]),
               to_hex(&self.to_bin()))
    }
}

impl Instr{
    pub fn new(op: Rc<Op>, a: Vec<u8>) -> Instr{
        Instr{op: op, argv: a, pos: 0}
    }

    pub fn arg(&self, i:usize) -> Arg{
        Arg{mode: self.op.argt[i].clone(), value: self.argv[i]}
    }

    pub fn len(&self) -> usize{
        1 + self.argv.len()
    }

    pub fn from_asm<'a,'b>(is: &'a InstrSet, asm: &'b str) -> Instr{
        let asm = asm.replace(","," ");
        let mut iter = asm.split_whitespace();
        if let Some(name) = iter.next(){
            let mut args = vec![];
            if let Some(s) = iter.next(){ args.push(Arg::from_asm(s)); }
            if let Some(s) = iter.next(){ args.push(Arg::from_asm(s)); }

            let (argt, argv) = Arg::split(&args);
            Instr::new(is.from_asm(name, argt), argv)
        } else { panic!("no input given") }
    }
    pub fn from_bin(is: &instr_set::InstrSet, opcode: u8) -> Instr{
        let op = is.from_bin(opcode);
        let argv = vec![0;op.argt.len()];
        Instr::new(op, argv)
    }

    pub fn to_asm(&self) -> String{
        let mut asm = self.op.name.to_owned();
        if self.argv.len() > 0 {
            let mut args = Vec::<String>::new();
            for i in 0..self.argv.len(){
                args.push(Arg::new(self.op.argt[i].clone(),self.argv[i]).to_asm());
            }
            asm = format!("{} {}",asm,args.join(", "));
        }
        asm
    }
    pub fn to_bin(&self) -> Vec<u8>{
        let mut data = vec![self.op.opcode];
        for v in &self.argv { data.push(v.clone()); }
        data
    }

    pub fn run(&self, vm: &mut vm::VM){
        &(self.op._run)(self, vm);
    }
}

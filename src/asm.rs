extern crate smvm_core;
#[macro_use] extern crate serde_derive;
extern crate docopt;

use smvm_core::asm;
use smvm_core::util::*;
use docopt::Docopt;
use std::fs;
use std::io;
use std::io::prelude::*;

const USAGE: &'static str = "
smvm-asm
An assember for Neil Bauer's Student Microprocessor Simulator.

Usage:
  smvm-asm (-h | --help)
  smvm-asm <file> [--print] [--lists] [--out=FILE]

Options:
  -h --help            Show this screen.
  -o FILE --out=FILE   Output to a binary file, use '-' to send to stderr
  -p --print           Print output as a hex table to stderr
  -l --lists           Print lists file to stderr
";

#[derive(Deserialize)]
struct Args{
    arg_file: String,
    flag_out: Option<String>,
    flag_print: bool,
    flag_lists: bool,
}

fn main(){
    let args: Args = Docopt::new(USAGE)
                    .and_then(|d| d.deserialize())
                    .unwrap_or_else(|e| e.exit());

    let file    = fs::File::open(args.arg_file).expect("Failed to load file");
    let reader  = io::BufReader::new(file);
    let mut asm = asm::Asm::new();
    let mut inp = Vec::<String>::new();

    for line in reader.lines() {
        inp.push(line.unwrap());
    }
    asm.parse(&inp);

    if let Some(f) = args.flag_out {
        if f == "-" {
           std::io::stdout().write(&asm.to_bin()[..]).expect("Failed to write to stdout");
        } else {
           let mut out = fs::File::create(f).expect("Failed to create output file");
           out.write(&asm.to_bin()[..]).expect("Failed to write to file");
        }
    }
    if args.flag_lists {
        let instr = asm.data();
        for i in instr{
            eprintln!("{}", i);
        }
    }
    if args.flag_print {
        eprintln!("{}", hex_table(&asm.to_bin()));
    }
}

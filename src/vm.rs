extern crate smvm_core;
#[macro_use] extern crate serde_derive;
extern crate docopt;
extern crate nix;

use nix::sys::termios;
use smvm_core::vm;
use docopt::Docopt;
use std::fs;
use std::io::prelude::*;
use std::time::Duration;

const USAGE: &'static str = "
smvm-vm
A Virtual Machine for Neil Bauer's Student Microprocessor Simulator.
Pass - as the file path to read from stdin.

Usage:
  smvm-asm (-h | --help)
  smvm-asm <file> [-rRea] [--clock=S] [--steps=N]

Options:
  -h --help            Show this screen.
  -a --asm             Print the ASM for each instruction before executing.
  -c S --clock=S       Set the clock speed in Hertz [default: 5.0]
  -e --end             Only output at the end of the program
  -r --registers       Output registers
  -R --ram             Output ram
  -s N --steps N       Run N instructions then stop
";

#[derive(Deserialize)]
struct Args {
    arg_file: String,
    flag_steps: Option<u32>,
    flag_registers: bool,
    flag_ram: bool,
    flag_end: bool,
    flag_asm: bool,
    flag_clock: f64,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                    .and_then(|d| d.deserialize())
                    .unwrap_or_else(|e| e.exit());

    let mut buf= vec![];

    if args.arg_file == "-" {
       std::io::stdin().read_to_end(&mut buf).expect("Failed to read from stdin");
    } else {
       let mut file = fs::File::open(args.arg_file).expect("Failed to open file");
       file.read_to_end(&mut buf).expect("Failed to read from file");
    }

    /*
    let saved_term = termios::tcgetattr(0).unwrap();
    let mut term = saved_term.clone();
    term.local_flags.remove(termios::LocalFlags::ICANON);
    term.local_flags.remove(termios::LocalFlags::ECHO);
    termios::tcsetattr(0, termios::SetArg::TCSADRAIN, &term).unwrap();
    */

    let mut vm = vm::VM::new();
    vm.load(&buf);

    let sleep = Duration::new(0,(1000000000f64 / args.flag_clock) as u32, );

    let mut step = 0;
    loop{
        let i = vm.step();
        if !args.flag_end{
            if args.flag_asm {
                if let Some(instr) = i { println!("{}", instr); }
            }
            if args.flag_ram { println!("{}",vm.ram_string()); }
            if args.flag_registers { println!("{}",vm.reg_string()); }
            if args.flag_ram || args.flag_registers{
                println!("");
            }
        }
        vm.disp_vdu();
        step += 1;
        if let Some(s) = args.flag_steps{
            if step == s { break; }
        }
        if vm.done { break; }
        vm.sleep(sleep);
    }

    if args.flag_end{
        if args.flag_ram { println!("{}",vm.ram_string()); }
        if args.flag_registers { println!("{}",vm.reg_string()); }
    }

    //termios::tcsetattr(0, termios::SetArg::TCSADRAIN, &saved_term).unwrap();
}

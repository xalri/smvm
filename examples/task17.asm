    jmp start

; Converts a null terminated string at [AL] to lowercase.
to_lower:
    org 02

    low_loop:
        mov BL, [AL]
        cmp BL, 00
        jz low_ret
        cmp BL, 41
        js low_next
        cmp BL, 5B
        jns low_next
        low_inc:
            add BL, 20
            mov [AL], BL
        low_next:
            inc AL
            jmp low_loop
    low_ret:
        ret

; Displays a null terminated string at [AL]
display:
    org 20
    mov BL, C0
    disp_loop:
        mov CL, [AL]
        cmp CL, 00
        jz disp_end
        mov [BL], CL
        inc AL
        inc BL
        jmp disp_loop
    disp_end:
        ret

start:
    mov BL, 50
loop:
    in 00
    mov [BL], AL
    inc BL
    cmp AL, 0D
    jnz loop

    mov AL, 50
    call 02
    mov AL, 50
    call 20
    end

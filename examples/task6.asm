    jmp start
    db "Hello World!"
    db 00

; Displays a null terminated string at [AL]
display:
    org 20
    mov BL, C0
    disp_loop:
        mov CL, [AL]
        cmp CL, 00
        jz disp_end
        mov [BL], CL
        inc AL
        inc BL
        jmp disp_loop
    disp_end:
        ret

start:
    mov AL, 02
    call 20
    end

; A simple clock for Neil Bauers' Student Microprocessor Simulator
; Counts from 1 to 99 on the dual 7 segment display using the hardware
; timer.
; As an optimisation, the digits are stored as the position in the programs
; memory which represents that digit.

; Registers:
; AL - Used to pass data to the display
; BL - Right digit of display + 3
; CL - Left digit of display + 3
; DL - The last value of BL which has been processed, used in the main loop
;      to check if we need to re-display (If BL hasn't changed we don't need
;      to redraw the display, so we keep waiting.

jmp start

db 0D  ; interupt handler location

db FA  ; 0  1111 1010  FA
db 0A  ; 1  0000 1010  0A
db B6  ; 2  1011 0110  B6
db 9E  ; 3  1001 1110  9E
db 4E  ; 4  0100 1110  4E
db DC  ; 5  1101 1100  DC
db FC  ; 6  1111 1100  FC
db 8A  ; 7  1000 1010  8A
db FE  ; 8  1111 1110  FE
db DE  ; 9  1101 1110  DE

; The interupt handler, increases the value of least significant digit
; of the clock then returns.
interupt:
    inc BL
    iret

; Shows the digits stored in BL and CL on the LED display
display:
    org  10
    mov  AL, [BL]
    or   AL, 01 ; Set the right hand digit
    out  02
    mov  AL, [CL]
    out  02
    ret

; If AL is > 9, set to 0 and increment BL
digitise:
    org 30
    cmp BL, 0D;
    js  digitize_end;
    sub BL, 0A
    inc CL
    digitize_end:
    ret

; The main entry point for the program.
start:
    sti
    mov BL, 03
    mov CL, 03
    mov DL, 03
    loop:
        cmp  BL, DL ; Check if BL has been changed by the interupt.
        jz   loop   ; If not, check again.
        call 30 ; Overflow BL into CL if needed
        push BL
        pop  DL
        call 10 ; Display the digits
        cmp  CL, 0D ; If we're over 99, reset the digits
        js   loop
    jmp start
    end

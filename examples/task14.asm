    jmp start
    db 30
    db 50
    db 90
    db 98
    db 84
    db 88
    db 90
    db D0

sleep:
    org 10
    wait_loop:
        dec AL
        cmp AL, 00
        jz wait_end
        jmp wait_loop
    wait_end:
        ret

start:
    mov BL, 02
loop:
    mov AL, [BL]
    out 01
    cmp BL, 02
    jz wait
    cmp BL ,06
    jnz next

    wait:
        mov AL, 08
        call 10

    next:
        inc BL
        cmp BL, 0A
        jz  start
        jmp loop
    end

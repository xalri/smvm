An assembler and virtual machine for Neil Bauers' [Student Microprocessor Simulator](http://www.softwareforeducation.com/sms32v50/).

The assembler is working but untested, the basic VM is mostly complete,
only the lights peripheral has been implemented so far.

Statically linked Linux x86\_64 binaries are included in the root of the repo,
to use, simply download, set as executable (chmod +x ./smvm-\*) and run from the terminal.

## Compilation

SMVM is written in rust, to compile you need rust and cargo installed.

    git clone git@github.com:xalri/smvm.git
    cd smvm
    cargo build --release

The binaries can then be found in ./target/release


## Assembler

    smvm-asm
    An assember for Neil Bauer's Student Microprocessor Simulator.

    Usage:
      smvm-asm (-h | --help)
      smvm-asm <file> [--print] [--lists] [--out=FILE]

    Options:
      -h --help            Show this screen.
      -o FILE --out=FILE   Output to a binary file
      -p --print           Print output as a hex table
      -l --lists           Print lists file

## Virtual Machine

    smvm-vm
    A Virtual Machine for Neil Bauer's Student Microprocessor Simulator.

    Usage:
      smvm-asm (-h | --help)
      smvm-asm <file> [-rRea] [--steps=N]

    Options:
      -h --help            Show this screen.
      -a --asm             Print the ASM for each instruction before executing.
      -e --end             Only output at the end of the program
      -r --registers       Output registers
      -R --ram             Output ram
      -s N --steps N       Run N instructions then stop
